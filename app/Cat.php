<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Cat extends Model
{
	use Sluggable;

	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

	public function getRouteKeyName()
    {
        return 'slug';
    }

    public function posts()
    {
    	return $this->hasMany('App\Post');
    }
}
