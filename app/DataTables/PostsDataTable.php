<?php

namespace App\DataTables;

use App\Post;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Services\DataTable;

class PostsDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        if (Auth::user()->is_admin === 1) {
            return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', function($row) {
                $url = url('post/'.$row->slug);
                $url_edit = url('posts/'.$row->id);
                $id = $row->id;

                return '<a href="'.$url.'" target="_blank" class="btn btn-xs btn-info"><i class="fa fa-eye" title="Show"></i></a>&nbsp;
                    <a href="'.$url_edit.'/edit" class="btn btn-row-edit btn-xs btn-warning"><i class="fa fa-pencil" title="Edit"></i></a>&nbsp;
                    <a href="javascript:;" onclick="swal_del('.$id.')"  class="btn btn-xs btn-danger"><i class="fa fa-trash" title="Delete"></i></a>';
            })
            ->editColumn('status_id', function($row) {
                $status_id = $row->status_id;
                switch ($status_id) {
                    case 1:
                        $label = '<span class="label label-success">Approved</span>';
                        break;
                    case 2:
                        $label = '<span class="label label-danger">Rejected</span>';
                        break;
                    case 3:
                        $label = '<span class="label label-primary">Draft</span>';
                        break;
                    default:
                        $label = '<span class="label label-warning">Pending</span>';
                        break;
                }

                return $label;
            })
            ->addColumn('publish', function($row) {
                $is_publish = $row->is_publish;
                $is_featured = $row->is_featured;
                $id = $row->id;

                return '<select onchange="publish(this, '.$id.')">
                            <option value="2"'.($is_publish == 1 && $is_featured == 1 ? ' selected' : '').'>Publish & Featured</option>
                            <option value="1"'.($is_publish == 1 && $is_featured == 0 ? ' selected' : '').'>Publish</option>
                            <option value="0"'.($is_publish == 0 ? ' selected' : '').'>Unpublish</option>
                        </select>';
            })
            ->editColumn('updated_at', function($row) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $row->updated_at)->formatLocalized('%d-%m-%Y %H:%M');
            })
            ->rawColumns(['status_id', 'publish', 'action'])
            ->make(true);
        }
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', function($row) {
                $url = url('post/'.$row->slug);
                $url_edit = url('posts/'.$row->id);
                $id = $row->id;
                if ($row->status_id === 1) {
                    return '';
                }

                return '<a href="'.$url.'" target="_blank" class="btn btn-xs btn-info"><i class="fa fa-eye" title="Show"></i></a>&nbsp;
                    <a href="'.$url_edit.'/edit" class="btn btn-row-edit btn-xs btn-warning"><i class="fa fa-pencil" title="Edit"></i></a>&nbsp;
                    <a href="javascript:;" onclick="swal_del('.$id.')"  class="btn btn-xs btn-danger"><i class="fa fa-trash" title="Delete"></i></a>';
            })
            ->editColumn('status_id', function($row) {
                $status_id = $row->status_id;
                switch ($status_id) {
                    case 1:
                        $label = '<span class="label label-success">Approved</span>';
                        break;
                    case 2:
                        $label = '<span class="label label-danger">Rejected</span>';
                        break;
                    case 3:
                        $label = '<span class="label label-primary">Draft</span>';
                        break;
                    default:
                        $label = '<span class="label label-warning">Pending</span>';
                        break;
                }

                return $label;
            })
            ->editColumn('updated_at', function($row) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $row->updated_at)->formatLocalized('%d-%m-%Y %H:%M');
            })
            ->rawColumns(['status_id', 'action'])
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        if (Auth::user()->is_admin === 1) {
            $query = Post::query()
                    ->select([
                        'posts.id as id',
                        'posts.title as title',
                        'posts.updated_at as updated_at',
                        'posts.status_id as status_id',
                        'posts.is_publish as is_publish',
                        'users.name as created_by',
                        'posts.note as note',
                        'posts.slug as slug',
                        'posts.is_featured as is_featured'
                    ])
                    ->where('status_id', '<>', '3')
                    ->leftJoin('users', 'posts.user_id', '=', 'users.id')
                    ->orderBy('posts.updated_at', 'desc');
        } else {
            $user_id = Auth::id();

            $query = Post::query()
                    ->select([
                        'posts.id as id',
                        'posts.title as title',
                        'posts.updated_at as updated_at',
                        'posts.status_id as status_id',
                        'posts.is_publish as is_publish',
                        'posts.note as note',
                        'posts.slug as slug'
                    ])
                    ->leftJoin('users', 'posts.user_id', '=', 'users.id')
                    ->where('posts.user_id', '=', $user_id)
                    ->orderBy('posts.updated_at', 'desc');
        }
        
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px', 'printable' => 'false'])
                    ->parameters([
                        'dom'          => 'Bfrtip',
                        'buttons'      => ['export', 'print', 'reset', 'reload'],
                        'responsive'    => true
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        if (Auth::user()->is_admin === 1) {
            return [
                'id',
                'title',
                'updated_at',
                'created_by',
                [
                    'name' => 'status_id',
                    'title' => 'Status',
                    'data' => 'status_id'
                ],
                'note',
                'publish'
            ];
        }
        return [
            'id',
            'title',
            'updated_at',
            [
                'name' => 'status_id',
                'title' => 'Status',
                'data' => 'status_id'
            ],
            'note',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'posts_' . time();
    }
}
