<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Analytics\Period;
use Analytics;

class AnalyticController extends Controller
{
    public function index()
    {
    	//fetch the most visited pages for today and the past week
		$mostVisited = Analytics::fetchMostVisitedPages(Period::days(7));

		//fetch visitors and page views for the past week
		$mostVisitor = Analytics::fetchVisitorsAndPageViews(Period::days(7));
		dd($mostVisited);
    }
}
