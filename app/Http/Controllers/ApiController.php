<?php

namespace App\Http\Controllers;

use App\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Unlu\Laravel\Api\QueryBuilder;

class ApiController extends Controller
{
    public function json()
    {
    	header('Access-Control-Allow-Origin: *');
			header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
			header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

			$result = [];

			$offset = request('offset', '0');
			$limit = request('limit', '20');

			$query = DB::table('posts')->select('id', 'title', 'photo', 'photo_ext')->limit(20)->offset($offset)->limit($limit)->get();
			foreach ($query as $data) {
				$data->datetime = 'Jumat, 06 Januari 2016';
				$data->photo = url('uploads/images/'.$data->photo.'_360x360.'.$data->photo_ext);
			}
			$result = $query;

			return response()->json([
		    'success' => true,
		    'data' => $result
			]);
    }

    public function item() 
    {
    	Carbon::setLocale('id');

			header('Access-Control-Allow-Origin: *');
			header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
			header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

			$result = [];
			$itemId = request('id', 1);

			$query = DB::table('posts')->select('id', 'title', 'photo', 'photo_ext', 'content', 'updated_at')->where('id', $itemId)->first();

			$query->photo = url('uploads/images/'.$query->photo.'_360x360.'.$query->photo_ext);
			$query->datetime = Carbon::createFromFormat('Y-m-d H:i:s', $query->updated_at)->formatLocalized('%A %d %B %Y');
			$result[] = $query;

			return response()->json([
		    'success' => true,
		    'data' => $result
			]);
    }

    public function posts(Request $request)
    {
    	header('Access-Control-Allow-Origin: *');
			header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
			header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
    	
    	$request->is_publish = 1;
    	$queryBuilder = new QueryBuilder(new Post, $request);

	    return response()->json([
	      'data' => $queryBuilder->build()->paginate()
	    ]);
    }
}
