<?php

namespace App\Http\Controllers;

use Socialite;
use Auth;
use App\User;

class FbAuthController extends Controller
{
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallback()
    {
        $user = Socialite::driver('facebook')->user();
        $data = [
        	'email' => $user->getEmail(),
        	'name' => $user->getName(),
        	'password' => 'OAuth'
        ];

        $user_check = User::where('email', $data['email'])->first();
        if (count($user_check) == 1) {
        	Auth::login($user_check);
        } else {
        	Auth::login(User::create($data));
        }
        return redirect()->to('/home');
    }
}
