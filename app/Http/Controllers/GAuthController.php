<?php

namespace App\Http\Controllers;

use Socialite;
use Auth;
use App\User;

class GAuthController extends Controller
{
    public function redirectToProvider()
    {
    		// Sends the user to Google
        return Socialite::driver('google')->redirect();
    }

    public function handleProviderCallback()
    {
    		// Google sends the status to us
        $user = Socialite::driver('google')->user();
        $data = [
        	'email' => $user->getEmail(),
        	'name' => $user->getName(),
        	'password' => 'OAuth'
        ];

        $user_check = User::where('email', $data['email'])->first();
        if (count($user_check) == 1) {
        	Auth::login($user_check);
        } else {
        	Auth::login(User::create($data));
        }
        return redirect()->to('/home');
    }
}
