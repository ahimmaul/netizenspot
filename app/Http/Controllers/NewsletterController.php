<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NewsletterController extends Controller
{
    public function subscribe(Request $request)
    {
    	DB::table('newsletter')->insert([
    		'email' => $request->email,
    		'created_at' => date('Y-m-d H:i:s'),
    		'status' => 1
    	]);
    	return response()->json([
			    'response' => 'success'
			]);
    }
}
