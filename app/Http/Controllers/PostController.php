<?php

namespace App\Http\Controllers;

use App\Cat;
use App\DataTables\PostsDataTable;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PostsDataTable $dataTable)
    {
        return $dataTable->render('posts.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $query = Cat::select('id', 'name')->get();
        $cats = [];
        foreach ($query as $data) {
            $cats[$data->id] = $data->name;
        }

        return view('posts.create', compact('cats'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
            'cat_id' => 'required',
            ]);
        $post = new Post;

        if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
            $path = $request->photo->store('temp');
            $filename = str_replace('temp/', '', $path);
            $path_explode = explode('.', $filename);
            $post->photo = $path_explode[0];
            $post->photo_ext = $path_explode[1];
            $img = Image::make(storage_path('app/temp/'.$filename));
            $img->resize(1024, 768);
            $img->save(storage_path('app/images/'.$filename));
            Storage::delete(storage_path('app/temp/'.$filename));
        }
        if ($request->has('save')) {
            $post->status_id = 3;
        } else $post->status_id = 0;

        $post->title = $request->title;
        $post->short_content = $request->short_content;
        $post->content = $request->content;
        $post->video = $request->video;
        $post->cat_id = $request->cat_id;
        $post->user_id = Auth::id();
        $post->updated_at = date('Y-m-d H:i:s');
        $post->save();

        return redirect('posts')->with('message', 'Post has been created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        $query = Cat::select('id', 'name')->get();
        $cats = [];
        foreach ($query as $data) {
            $cats[$data->id] = $data->name;
        }
        $post->photo = url('uploads/images/'.$post->photo.'_450x258.'.$post->photo_ext);

        return view('posts.detail', ['post' => $post, 'cats' => $cats]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $query = Cat::select('id', 'name')->get();
        $cats = [];
        foreach ($query as $data) {
            $cats[$data->id] = $data->name;
        }
        $post->photo = url('uploads/images/'.$post->photo.'_450x258.'.$post->photo_ext);

        return view('posts.edit', ['post' => $post, 'cats' => $cats]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
            'cat_id' => 'required',
            ]);
        $post = Post::find($id);

        if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
            $path = $request->photo->store('temp');
            $filename = str_replace('temp/', '', $path);
            $path_explode = explode('.', $filename);
            $post->photo = $path_explode[0];
            $post->photo_ext = $path_explode[1];
            $img = Image::make(storage_path('app/temp/'.$filename));
            $img->resize(1024, 768);
            $img->save(storage_path('app/images/'.$filename));
            Storage::delete(storage_path('app/temp/'.$filename));
        }
        if ($request->has('save')) {
            $post->status_id = 3;
            $post->updated_at = date('Y-m-d H:i:s');
        } else if ($request->has('approve_publish')) {
            $post->is_publish = 1;
            $post->status_id = 1;
        } else if ($request->has('approve')) {
            $post->status_id = 1;
        } else if ($request->has('denied')) {
            $post->status_id = 2;
        } else if ($request->has('submit')) {
            $post->status_id = 0;
            $post->updated_at = date('Y-m-d H:i:s');
        }

        $post->slug = null;
        $post->title = $request->title;
        $post->short_content = $request->short_content;
        $post->content = $request->content;
        $post->video = $request->video;
        $post->cat_id = $request->cat_id;
        $post->note = $request->note;
        $post->save();

        return redirect('posts')->with('message', 'Post has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        $post->delete();

        return redirect('posts')->with('message', 'Post has been deleted');
    }

    public function publish($id, $is_publish)
    {
        $post = Post::findOrFail($id);
        if ($is_publish == 2) {
            $post->is_featured = 1;
            $post->is_publish = 1;
        } else if ($is_publish == 1) {
            $post->is_publish = 1;
            $post->status_id = 1;
            $post->is_featured = 0;
        } else {
            $post->is_publish = 0;
        }
        $post->save();

        return redirect('posts');
    }
}
