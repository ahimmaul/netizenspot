<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class SettingController extends Controller
{
    public function __construct() 
    {
    	$this->middleware('auth');
    }

    public function edit()
    {
    	$user = Auth::user()->profile;

    	return view('settings.edit', compact('user'));
    }

    public function update(Request $request)
    {
    	$this->validate($request, [
            'name' => 'required',
            'birthday' => 'required',
            'phone' => 'required',
            'address' => 'required'
            ]);
    	$data = [
    		'user_id' => Auth::id(),
    		'birthday' => $request->birthday,
    		'phone' => $request->phone,
    		'address' => $request->address,
    	];
        $profile = Profile::updateOrCreate(
            ['user_id' => Auth::id()],
            ['birthday' => $request->birthday,
                'phone' => $request->phone,
                'address' => $request->address]
        );
        $user = Auth::user();
        $user->name = $request->name;
        $user->save();

        return redirect('settings/edit')->with('message', 'Profile has been updated');
    }

    public function avatar()
    {
        $user = Auth::user();

    	return view('settings.avatar', compact('user'));
    }

    public function avatar_update(Request $request)
    {
        $this->validate($request, [
                'photo' => 'required'
            ]);

        if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
            $path = $request->photo->store('temp');

            $filename = str_replace('temp/', '', $path);
            $path_explode = explode('.', $filename);

            $img = Image::make(storage_path('app/temp/'.$filename));
            $img->resize(1024, 1024);
            $img->save(storage_path('app/images/'.$filename));

            Storage::delete(storage_path('app/temp/'.$filename));

            $user = Auth::user();
            $user->avatar = $path_explode[0];
            $user->avatar_ext = $path_explode[1];
            $user->save();
        }

        return redirect('settings/avatar')->with('message', 'Profile has been updated');
    }

    public function password()
    {
        return view('settings.password');
    }

    public function password_update(Request $request)
    {
        if (Auth::user()->password <> 'OAuth') {
            $this->validate($request, [
                'password' => 'required|min:6|confirmed|different:password_old',
            ]);
        } else {
            $this->validate($request, [
                'password_old' => 'required',
                'password' => 'required|min:6|confirmed|different:password_old',
            ]);

            $user = Auth::user();
            if (!Hash::check($request->password_old, $user->password)) {
                return back()->with('error', 'The specified password does not match the database password');
            }
        }

        $user->password = bcrypt($request->password);
        $user->save();

        return redirect('settings/password')->with('message', 'Password has been updated');
    }
}
