<?php

namespace App\Http\Controllers;

use App\Cat;
use App\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Jenssegers\Agent\Agent;
use SEO;

class WelcomeController extends Controller
{
    public function index()
    {
        setlocale(LC_ALL, 'IND');

        SEO::setTitle('Netizenspot - Portal Berita Terupdate Hari ini');
        SEO::setDescription('Indeks berita terbaru hari ini dari peristiwa, kecelakaan, kriminal, hukum, berita unik, Politik, dan liputan khusus di Indonesia dan Internasional');
        SEO::opengraph()->setUrl(url('/'));
        SEO::setCanonical(url('/'));
        SEO::opengraph()->addProperty('type', 'articles');
        SEO::twitter()->setSite('@netizenspot');

        $posts_berita = DB::table('posts')
            ->join('cats', 'posts.cat_id', '=', 'cats.id')
            ->select('posts.*', 'cats.name as category', 'cats.slug as cat_slug')
            ->where([
                'posts.is_publish' => 1,
                'posts.deleted_at' => null
                ])->orderBy('updated_at', 'desc')->limit(14)->get();
        foreach ($posts_berita as $post) {
            $post->photo = (empty($post->photo) ? url('static/images/watermark_595x376.png') : url('uploads/images/'.$post->photo.'_595x376.'.$post->photo_ext));
            $post->datetime = Carbon::createFromFormat('Y-m-d H:i:s', $post->updated_at)->formatLocalized('%A, %d %b %Y');
            $post->link = url('post/'.$post->slug);
        }

        // featured post
        $posts_featured = DB::table('posts')
            ->join('cats', 'posts.cat_id', '=', 'cats.id')
            ->select('posts.*', 'cats.name as category', 'cats.slug as cat_slug')
            ->where([
                'posts.is_featured' => 1,
                'posts.is_publish' => 1,
                'posts.deleted_at' => null
                ])->orderBy('updated_at', 'desc')->limit(6)->get();
        foreach ($posts_featured as $post) {
            $post->photo = (empty($post->photo) ? url('static/images/watermark_595x376.png') : url('uploads/images/'.$post->photo.'_595x376.'.$post->photo_ext));
            $post->datetime = Carbon::createFromFormat('Y-m-d H:i:s', $post->updated_at)->formatLocalized('%A, %d %b %Y');
            $post->link = url('post/'.$post->slug);
        }

        $posts_featured_last = DB::table('posts')
            ->join('cats', 'posts.cat_id', '=', 'cats.id')
            ->select('posts.*', 'cats.name as category', 'cats.slug as cat_slug')
            ->where([
                'posts.is_featured' => 1,
                'posts.is_publish' => 1,
                'posts.deleted_at' => null
                ])->orderBy('updated_at', 'desc')->offset(6)->limit(4)->get();
        foreach ($posts_featured_last as $post) {
            $post->photo = (empty($post->photo) ? url('static/images/watermark_595x376.png') : url('uploads/images/'.$post->photo.'_595x376.'.$post->photo_ext));
            $post->datetime = Carbon::createFromFormat('Y-m-d H:i:s', $post->updated_at)->formatLocalized('%A, %d %b %Y');
            $post->link = url('post/'.$post->slug);
        }

        // popular post
        $posts_popular = DB::table('posts')
            ->join('cats', 'posts.cat_id', '=', 'cats.id')
            ->select('posts.*', 'cats.name as category', 'cats.slug as cat_slug')
            ->where([
                'posts.is_publish' => 1,
                'posts.deleted_at' => null,
                'posts.is_popular' => 1
                ])->offset(2)->limit(5)->orderBy('updated_at', 'desc')->get();
        foreach ($posts_popular as $post) {
            $post->photo = (empty($post->photo) ? url('static/images/watermark_95x95.png') : url('uploads/images/'.$post->photo.'_95x95.'.$post->photo_ext));
            $post->datetime = Carbon::createFromFormat('Y-m-d H:i:s', $post->updated_at)->formatLocalized('%A, %d %b %Y');
            $post->link = url('post/'.$post->slug);
        }

        $posts_popular_first = DB::table('posts')
            ->join('cats', 'posts.cat_id', '=', 'cats.id')
            ->select('posts.*', 'cats.name as category', 'cats.slug as cat_slug')
            ->where([
                'posts.deleted_at' => null,
                'posts.is_publish' => 1,
                'posts.is_popular' => 1
                ])->limit(1)->orderBy('updated_at', 'desc')->get();
        foreach ($posts_popular_first as $post) {
            $post->photo = (empty($post->photo) ? url('static/images/watermark_595x376.png') : url('uploads/images/'.$post->photo.'_595x376.'.$post->photo_ext));
            $post->updated_at = Carbon::createFromFormat('Y-m-d H:i:s', $post->updated_at)->formatLocalized('%A, %d %b %Y');
            $post->link = url('post/'.$post->slug);
        }

        // video post
        $posts_video = Post::with('user', 'cat')->whereNotNull('video')->where('video', '<>', '')->where('is_publish', 1)->limit(6)->orderBy('updated_at', 'desc')->get();
        foreach ($posts_video as $post) {
            $post->photo = (empty($post->photo) ? url('static/images/watermark_200x130.png') : url('uploads/images/'.$post->photo.'_200x130.'.$post->photo_ext));
            $post->link = url('post/'.$post->slug);
            $post->datetime = Carbon::createFromFormat('Y-m-d H:i:s', $post->updated_at)->formatLocalized('%A, %d %b %Y');
        }

        // categories
        $cats = Cat::all();
        foreach ($cats as $cat) {
            // latest post
            $posts_latest[$cat->id] = DB::table('posts')
                ->join('cats', 'posts.cat_id', '=', 'cats.id')
                ->select('posts.*', 'cats.name as category', 'cats.slug as cat_slug')
                ->where([
                    'posts.deleted_at' => null,
                    'posts.is_publish' => 1,
                    'posts.cat_id' => $cat->id
                ])->limit(4)->orderBy('updated_at', 'desc')->get();
            foreach ($posts_latest[$cat->id] as $post) {
                $post->photo = (empty($post->photo) ? url('static/images/watermark_200x138.png') : url('uploads/images/'.$post->photo.'_200x138.'.$post->photo_ext));
                $post->updated_at = Carbon::createFromFormat('Y-m-d H:i:s', $post->updated_at)->formatLocalized('%A, %d %b %Y');
                $post->link = url('post/'.$post->slug);
            }
        }
        
        $agent = new Agent();
        if ($agent->isMobile()) {
            return view('welcome.index_mobile', ['posts_featured' => $posts_featured, 'cats' => $cats, 'posts_latest' => $posts_latest, 'posts_popular' => $posts_popular, 'posts_popular_first' => $posts_popular_first, 'posts_video' => $posts_video, 'posts_berita' => $posts_berita, 'posts_featured_last' => $posts_featured_last]);
        } else {
            return view('welcome.index', ['posts_featured' => $posts_featured, 'cats' => $cats, 'posts_latest' => $posts_latest, 'posts_popular' => $posts_popular, 'posts_popular_first' => $posts_popular_first, 'posts_video' => $posts_video, 'posts_berita' => $posts_berita, 'posts_featured_last' => $posts_featured_last]);
        }

    }

    public function post(Request $request, Post $post)
    {
        $cats = Cat::all();
        $post->photo = (empty($post->photo) ? url('static/images/watermark_525x350.png') : url('uploads/images/'.$post->photo.'_525x350.'.$post->photo_ext));
        $post->day = Carbon::createFromFormat('Y-m-d H:i:s', $post->updated_at)->formatLocalized('%d');
        $post->month = Carbon::createFromFormat('Y-m-d H:i:s', $post->updated_at)->formatLocalized('%B');
        $post->year = Carbon::createFromFormat('Y-m-d H:i:s', $post->updated_at)->formatLocalized('%Y');
        $other = Post::with('user', 'cat')->find($post->id);
        $post->created_by = $other->user->name;
        $post->category = $other->cat->name;
        $post->category_slug = $other->cat->slug;

        // popular post
        $posts_popular = DB::table('posts')
            ->join('cats', 'posts.cat_id', '=', 'cats.id')
            ->select('posts.*', 'cats.name as category', 'cats.slug as cat_slug')
            ->where([
                'posts.deleted_at' => null,
                'posts.is_publish' => 1,
                'posts.is_popular' => 1
                ])->limit(10)->orderBy('updated_at', 'desc')->get();
        foreach ($posts_popular as $data) {
            $data->photo = (empty($data->photo) ? url('static/images/watermark_95x95.png') : url('uploads/images/'.$data->photo.'_95x95.'.$data->photo_ext));
            $data->updated_at = Carbon::createFromFormat('Y-m-d H:i:s', $data->updated_at)->formatLocalized('%A, %d %b %Y');
            $data->link = url('post/'.$data->slug);
        }
        return view('welcome.post', ['post' => $post, 'cats' => $cats, 'posts_popular' => $posts_popular]);
    }

    public function category(Request $request, Cat $cat)
    {
        $cats = Cat::all();
        $posts = Post::with('user', 'cat')->where('cat_id', $cat->id)->where('is_publish', 1)->orderBy('updated_at', 'desc')->paginate(5);
        foreach ($posts as $post) {
            $post->day = Carbon::createFromFormat('Y-m-d H:i:s', $post->updated_at)->formatLocalized('%d');
            $post->month = Carbon::createFromFormat('Y-m-d H:i:s', $post->updated_at)->formatLocalized('%B');
            $post->year = Carbon::createFromFormat('Y-m-d H:i:s', $post->updated_at)->formatLocalized('%Y');
            $post->photo = (empty($post->photo) ? url('static/images/watermark_525x350.png') : url('uploads/images/'.$post->photo.'_525x350.'.$post->photo_ext));
            $post->link = url('post/'.$post->slug);
        }

        // popular post
        $posts_popular = DB::table('posts')
            ->join('cats', 'posts.cat_id', '=', 'cats.id')
            ->select('posts.*', 'cats.name as category', 'cats.slug as cat_slug')
            ->where([
                'posts.deleted_at' => null,
                'posts.is_publish' => 1,
                'posts.is_popular' => 1
                ])->limit(10)->orderBy('updated_at', 'desc')->get();
        foreach ($posts_popular as $post) {
            $post->photo = (empty($post->photo) ? url('static/images/watermark_95x95.png') : url('uploads/images/'.$post->photo.'_95x95.'.$post->photo_ext));
            $post->updated_at = Carbon::createFromFormat('Y-m-d H:i:s', $post->updated_at)->formatLocalized('%A, %d %b %Y');
            $post->link = url('post/'.$post->slug);
        }

        return view('welcome.category', ['posts' => $posts, 'cats' => $cats, 'posts_popular' => $posts_popular]);
    }
}
