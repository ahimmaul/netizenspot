jQuery(function($) {

    'use strict';

    /*==============================================================*/
    // Table of index
    /*==============================================================*/

    /*==============================================================
    # sticky-nav
    # Date Time
    # language Select
		# Search Slide
		# Breaking News
		# Owl Carousel
		# magnificPopup
		# newsletter
		# weather	
	
    ==============================================================*/




    /*==============================================================*/
    // # sticky-nav
    /*==============================================================*/
    (function() {
        var windowWidth = $(window).width();
        if (windowWidth > 1000) {
            $(window).scroll(function() {
                var sT = $(this).scrollTop();
                if (sT >= 120) {
                    $('.homepage .navbar, .homepage-two.fixed-nav .navbar').addClass('sticky-nav')
                } else {
                    $('.homepage .navbar, .homepage-two.fixed-nav .navbar').removeClass('sticky-nav')
                };
            });
        } else {
            $('.homepage .navbar, .homepage-two.fixed-nav .navbar').removeClass('sticky-nav')
        };
        if (windowWidth > 1000) {
            $(window).scroll(function() {
                var sT = $(this).scrollTop();
                if (sT >= 120) {
                    $('.homepage #menubar, .homepage-two.fixed-nav #navigation').removeClass('container')
                    $('.homepage #menubar, .homepage-two.fixed-nav #navigation').addClass('container-fluid')
                } else {
                    $('.homepage #menubar, .homepage-two.fixed-nav #navigation').removeClass('container-fluid')
                    $('.homepage #menubar').addClass('container')
                }
            });
        } else {
            $('.homepage #menubar, .homepage-two.fixed-nav #navigation').removeClass('container-fluid')
        };

    }());



    /*==============================================================*/
    // # Date Time
    /*==============================================================*/

    (function() {

        var datetime = null,
            date = null;
        var update = function() {
            date = moment(new Date())
            datetime.html(date.format('dddd, MMMM D,  YYYY'));
        };
        datetime = $('#date-time')
        update();
        setInterval(update, 1000);

    }());

    /*==============================================================*/
    // sticky
    /*==============================================================*/
    (function() {
        $("#sticky").stick_in_parent();
    }());

});

/*==============================================================*/
// Weather
/*==============================================================*/
// Docs at http://simpleweatherjs.com

$(document).ready(function() {
    if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(function(position) {
		        loadWeather(position.coords.latitude + ',' + position.coords.longitude); //load weather using your lat/lng coordinates
		    });
    } else {
        loadWeather('london, uk', '');
    }
});

function loadWeather(location, woeid) {
    $.simpleWeather({
        location: location,
        woeid: woeid,
        unit: 'c',
        success: function(weather) {
            html = '<span>' + weather.city + ' </span><img src="' + weather.thumbnail + '"><span> ' + weather.temp + '&deg;' + weather.units.temp + '</span>';
        		$("#weather").html(html);
        },
        error: function(error) {
            $("#weather").html('<p>' + error + '</p>');
        }
    });
}