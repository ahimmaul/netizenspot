
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('../../bower/jquery-easing/jquery-easing.min.js');
require('../../bower/jRespond/jRespond.min.js');
require('../../bower/smooth-scroll/smooth-scroll.min.js');
require('../../bower/jquery-appear/jquery.appear.min.js');
require('../../bower/animsition/dist/js/animsition.min.js');
require('../../bower/isotope/dist/isotope.pkgd.min.js');
require('../../bower/fitvids/jquery.fitvids.js');
require('../../bower/vide/dist/jquery.vide.min.js');
require('../../bower/magnific-popup/dist/jquery.magnific-popup.min.js');
require('../../bower/jquery-countTo/jquery.countTo.js');
require('../../bower/dist/jquery.easypiechart.min.js');
require('../../bower/jquery.gmap.min.js');
require('../../bower/jquery.form.js');
require('../../bower/jquery.validate.min.js');
require('../../bower/dist/bootstrap-notify.min.js');
require('../../bower/dist/jquery.mb.YTPlayer.min.js');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/*Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
    el: '#app'
});
*/