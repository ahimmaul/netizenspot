@extends('layouts.auth')

@section('title', 'Log In')
@section('content')
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="{{ url('/') }}"><b>Netizen</b>Spot</a>
  </div>
  <div class="login-box-body">
    @include('partials.message')
    <p class="login-box-msg">Sign in to start your session</p>

    <form action="{{ url('/login') }}" method="POST">
      {{ csrf_field() }}
      <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
        <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
        <input type="password" class="form-control" placeholder="Password" name="password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Remember Me
            </label>
          </div>
        </div>

        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
      </div>
    </form>

    <a href="{{ url('/password/reset') }}">I forgot my password</a><br>
    <a href="{{ url('register') }}" class="text-center">Register a new membership</a>
    
  </div>
  <br>
  <div class="login-box-body">
    <p class="text-center">OR</p>
    <a class="btn btn-block btn-social btn-facebook" href="{{ url('auth/fb') }}">
      <i class="fa fa-facebook"></i> Sign in with Facebook
    </a>
    <a class="btn btn-block btn-social btn-google" href="{{ url('auth/google') }}">
      <i class="fa fa-google-plus"></i> Sign in with Google
    </a>
  </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset ("/bower_components/admin-lte/plugins/iCheck/icheck.min.js") }}"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%'
    });
  });
</script>
@endsection

@section('styles')
<style>
  .login-box-body {
    box-shadow: 0px 0px 25px #999999;
  }
</style>
@endsection
