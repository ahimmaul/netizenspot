@extends('layouts.auth')

@section('title', 'Sign Up')
@section('content')
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="{{ url('/') }}"><b>Netizen</b>Spot</a>
  </div>
  <div class="register-box-body">
    <p class="login-box-msg">Register a new membership</p>

    <form action="{{ url('/register') }}" method="POST">
      {{ csrf_field() }}
      <div class="form-group has-feedback{{ $errors->has('name') ? ' has-error' : '' }}">
        <input type="text" class="form-control" placeholder="Full name" name="name" value="{{ old('name') }}" required autofocus>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>

        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
        <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
        <input type="password" class="form-control" placeholder="Password" name="password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>

        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Retype password" name="password_confirmation" required>
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div>
      {!! Recaptcha::render([ 'lang' => 'en' ]) !!}
      <div class="row">
        <div class="col-xs-8">
          <a href="{{ url('login') }}" class="text-center">I already have a membership</a>
        </div>
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
      </div>
    </form>
  </div>
  <br>
  <div class="login-box-body">
    <p class="text-center">OR</p>
    <a class="btn btn-block btn-social btn-facebook" href="{{ url('auth/fb') }}">
      <i class="fa fa-facebook"></i> Sign in with Facebook
    </a>
    <a class="btn btn-block btn-social btn-google" href="{{ url('auth/google') }}">
      <i class="fa fa-google-plus"></i> Sign in with Google
    </a>
  </div>
</div>
@endsection

@section('styles')
<style>
  .register-box-body {
    box-shadow: 0px 0px 25px #999999;
  }
</style>
@endsection