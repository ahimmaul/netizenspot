<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name') }}</title>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-TDKXX2Q');</script>
    <!-- End Google Tag Manager -->
    {!! SEO::generate(true) !!}
    <link rel="shortcut icon" href="{{ url('static/images/ico_150x150.png') }}">
    <link rel="stylesheet" href="{{ url('css/app.css') }}">
    <link rel="stylesheet" href="{{ url('css/all.css') }}">
    <link rel="stylesheet" href="{{ url('css/non-responsive.css') }}">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ url('bower_components/slicknav/dist/slicknav.min.css') }}" />

    <!--[if lt IE 9]>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js') }}"></script>
    <![endif]-->    
    @yield('styles')
    <style type="text/css">
        .slicknav_menu {
            display:block;
            background: #d82731;
        }

        #topbar {
            box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
            width: 100%;
            position: fixed;
        }
        #mainMenu ul li a:hover {
            background-color: #f44336;
            color: #000;
        }
        #mainMenu ul li a {
            color: #fff;
        }
        #mainMenu {
            float: none;
            margin: 0 auto;
            display: none;
        }
        #header {
            background-color: #ececec;
        }
        .navigation-wrap {
            background-color: #d82731;
        }
        #logo {
            margin-top: 45px;
        }
        .topbar-dropdown {
            background-color: #d82731;
        }
        .topbar-dropdown:hover {
            background-color: #f44336;
        }
        .button-login {
            color: #ffffff !important;
        }
        .container {
            padding-left: 0px;
            padding-right: 0px;
        }
        .panel-primary>.panel-heading {
            background-color: #d82731;
            padding-bottom: 0px;
            border-color: #ececec;
        }
        .panel-primary {
            border-color: #ececec;
        }
        .panel-default>.panel-heading {
            background-color: #2a2f4d !important;
            padding-bottom: 0px;
        }
        .panel-heading h4 {
            color: #fff;
        }
        h2 a {
            line-height: 120% !important;
        }
    </style>
</head>
<body class="wide no-page-loader">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TDKXX2Q"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div class="wrapper">
        <div id="topbar" class="">
            <div class="container">
                <div class="hidden-xs" style="float:left !important">
                    <div class="social-icons social-icons-colored-hover">
                        <ul>
                            <li class="social-facebook"><a href="https://www.facebook.com/netizenspot/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li class="social-twitter"><a href="https://twitter.com/netizenspotnews" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li class="social-instagram"><a href="https://www.instagram.com/netizenspot/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <li class="social-youtube"><a href="https://www.youtube.com/channel/UCYdRR8OFG5vxHo7wF3ZRxXw" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="topbar-dropdown pull-right" id="login">
                    <div class="title"><i class="fa fa-user button-login"></i><a href="{{ url('login') }}" class="button-login">Masuk</a></div>
                </div>
            </div>
        </div>
        <div class="im-centered" style="background-color: #ececec;">
            <div class="row" id="top-logo" style="background-color: #ececec;width: 1000px; margin: 0 auto;padding-top: 20px">
                <div class="col-md-12" style="margin-top: 10px;padding-left: 5px;">
                    <a href="{{ url('/') }}">
                        <img src="{{ url('static/images/net_300x50.png') }}" id="logo" alt="Netizenspot Logo" width="400px" style="margin-left: auto; margin-right: auto;">
                    </a>
                </div>
            </div>
        </div>
        <header id="header" class="header-mini header-no-sticky">
            <div id="header-wrap">
                <div class="container">
                    <div class="navigation-wrap">
                        <div class="container">
                            <nav id="mainMenu" class="main-menu mega-menu">
                                <ul class="main-menu nav nav-pills" id="menu" style="width: 1200px;padding-left: 60px">
                                    @if(Request::is('/'))
                                    <li class="active">
                                    @else
                                    <li>
                                    @endif
                                        <a href="{{ url('/') }}">Home</a>
                                    </li>
                                @foreach ($cats as $cat)
                                    @if(Request::is('category/'.$cat->slug))
                                    <li class="active">
                                    @else
                                    <li>
                                    @endif
                                        <a href="{{ url('category/'.$cat->slug) }}">{{ $cat->name }}</a>
                                    </li>
                                @endforeach
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        @yield('content')
        <footer class="background-dark text-grey" id="footer">
            <div class="copyright-content">
                <div class="container">
                    <div class="row">
                        <div class="copyright-text col-md-6"> &copy; 2016 Netizenspot Team.
                        </div>
                        <div class="col-md-6">
                            <div class="social-icons">
                                <ul>
                                    <li class="social-facebook"><a href="https://www.facebook.com/netizenspot/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                    <li class="social-twitter"><a href="https://twitter.com/netizenspotnews" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                    <li class="social-instagram"><a href="https://www.instagram.com/netizenspot/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                    <li class="social-youtube"><a href="https://www.youtube.com/channel/UCYdRR8OFG5vxHo7wF3ZRxXw" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

    </div>
    <a class="gototop gototop-button" href="#top"><i class="fa fa-chevron-up"></i></a>
    <script src="{{ url('bower_components/jquery/dist/jquery.js') }}"></script>
    <script src="{{ url('bower_components/slicknav/dist/jquery.slicknav.min.js') }}"></script>
    @yield('scripts')
    <script type="text/javascript">
        $("a[href='#top']").click(function() {
          $("html, body").animate({ scrollTop: 0 }, "slow");
          return false;
        });
        $("#login").click(function() {
            location.href="{{ url('/login') }}";
        });
        $(function(){
            $('#menu').slicknav({prependTo: 'header'});
        });
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-58049176-7', 'auto');
        ga('send', 'pageview');
    </script>
</body>
</html>
