@if (Session::has('message'))
<div class='alert alert-success'>
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <h4><i class="icon fa fa-check"></i> {{ Session::get('message') }}</h4>
</div>
@endif
@if (Session::has('error'))
<div class='alert alert-danger'>
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <h4><i class="icon fa fa-ban"></i> Error</h4>
  <ul>
    <li>{{ Session::get('error') }}</li>
  </ul>
</div>
@endif