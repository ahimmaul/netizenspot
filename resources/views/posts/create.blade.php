@extends('layouts.app')

@section('content')
<section class="content-header">
  <h1>
    Posts
    <small>Add New</small>
  </h1>
</section>

<section class="content">
  @include('partials.error')
  <div class="row">
    <div class="col-md-12">
       <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Form Post</h3>
        </div>
        {!! Form::open(['url' => 'posts', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
          <div class="box-body">
            <div class="form-group">
              {!! Form::label('title', 'Title', ['class' => 'col-sm-2 control-label']) !!}
              <div class="col-sm-10">
                {!! Form::text('title', old('title'), ['class' => 'form-control', 'required' => 'required', 'maxlength' => '100', 'placeholder' => 'Max Length 100']) !!}
              </div>
            </div>
            <div class="form-group">
              {!! Form::label('short_content', 'Short Content', ['class' => 'col-sm-2 control-label']) !!}
              <div class="col-sm-10">
                {!! Form::textarea('short_content', old('short_content'), ['class' => 'form-control', 'required' => 'required', 'rows' => 2, 'cols' => '50', 'maxlength' => '150', 'placeholder' => 'Max Length 200']) !!}
              </div>
            </div>
            <div class="form-group">
              {!! Form::label('content', 'Content', ['class' => 'col-sm-2 control-label']) !!}
              <div class="col-sm-10">
                {!! Form::textarea('content', old('content'), ['class' => 'form-control', 'required' => 'required', 'id' => 'content_editor', 'rows' => 10, 'cols' => 80]) !!}
              </div>
            </div>
            <div class="form-group">
              {!! Form::label('photo', 'Photo', ['class' => 'col-sm-2 control-label']) !!}
              <div class="col-sm-10">
                {!! Form::file('photo', ['class' => 'form-control']) !!}
              </div>
            </div>
            <div class="form-group">
              {!! Form::label('video', 'Video', ['class' => 'col-sm-2 control-label']) !!}
              <div class="col-sm-10">
                {!! Form::text('video', old('video'), ['class' => 'form-control', 'placeholder' => 'Youtube ID']) !!}
              </div>
            </div>
            <div class="form-group">
              {!! Form::label('cat_id', 'Category', ['class' => 'col-sm-2 control-label']) !!}
              <div class="col-sm-10">
                {!! Form::select('cat_id', $cats, null, ['placeholder' => 'Category', 'class' => 'form-control']) !!}
              </div>
            </div>
          </div>
          <div class="box-footer">
            {!! Form::submit('Submit', ['class' => 'btn btn-success pull-right', 'name' => 'publish']) !!}
            {!! Form::submit('Save Draft', ['class' => 'btn btn-default', 'name' => 'save']) !!}
          </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</section>
@endsection

@section('scripts')
<script src="{{ url('bower_components/ckeditor/ckeditor.js') }}"></script>
<script src="{{ url('bower_components/ckeditor/config.js') }}"></script>
<script src="{{ url('bower_components/jquery.are-you-sure/jquery.are-you-sure.js') }}"></script>
<script src="{{ url('vendor/laravel-filemanager/js/lfm.js') }}"></script>
<script type="text/javascript">
  CKEDITOR.replace('content_editor', {
    filebrowserImageBrowseUrl: '/filemanager?type=Images',
    filebrowserImageUploadUrl: '/filemanager/upload?type=Images&_token={{csrf_token()}}',
    filebrowserBrowseUrl: '/filemanager?type=Files',
    filebrowserUploadUrl: '/filemanager/upload?type=Files&_token={{csrf_token()}}'
  });
  $('#lfm').filemanager('image');
  $('form').areYouSure({'message':'Your post are not saved!'});
</script>
@endsection