@extends('layouts.app')

@section('content')
<section class="content-header">
  <h1>
    Posts
    <small>Detail</small>
  </h1>
</section>

<section class="content">
  @include('partials.error')
  <div class="row">
    <div class="col-md-12">
       <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Form Post</h3>
        </div>
        {!! Form::open(['url' => 'posts', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
          <div class="box-body">
            <div class="form-group">
              {!! Form::label('title', 'Title', ['class' => 'col-sm-2 control-label']) !!}
              <div class="col-sm-10">
                {!! Form::text('title', $post->title, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
              </div>
            </div>
            <div class="form-group">
              {!! Form::label('content', 'Content', ['class' => 'col-sm-2 control-label']) !!}
              <div class="col-sm-10">
                {!! Form::textarea('content', $post->content, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
              </div>
            </div>
            <div class="form-group">
              {!! Form::label('photo', 'Photo', ['class' => 'col-sm-2 control-label']) !!}
              <div class="col-sm-10">
                @if (isset($post->photo))
                <img src="{{ $post->photo }}"/>
                @endif
                {!! Form::file('photo', ['class' => 'form-control', 'readonly' => 'readonly']) !!}
              </div>
            </div>
            <div class="form-group">
              {!! Form::label('video', 'Video', ['class' => 'col-sm-2 control-label']) !!}
              <div class="col-sm-10">
                {!! Form::text('video', $post->video, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
              </div>
            </div>
            <div class="form-group">
              {!! Form::label('cat_id', 'Category', ['class' => 'col-sm-2 control-label']) !!}
              <div class="col-sm-10">
                {!! Form::text('cat_id', $cats[$post->cat_id], ['class' => 'form-control', 'readonly' => 'readonly']) !!}
              </div>
            </div>
          </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</section>
@endsection
