@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
    <link href="{{ asset("/css/sweetalert.min.css")}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<section class="content-header">
  <h1>
    Posts
    <a href="{{ url('posts/create') }}" class="btn btn-default">Add New</a>
  </h1>
</section>

<section class="content">
  @include('partials.message')
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-body">
          <div class="table-responsive">
            {!! $dataTable->table([], true) !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
  function form_post(path, method) {
    var params = [];
    params["_token"] = "{!! csrf_token() !!}";
    params["_method"] = method;
    //{"_token": "{!! csrf_token() !!}", "_method": method};

    var form = document.createElement("form");
    form.setAttribute("method", "POST");
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
         }
    }

    document.body.appendChild(form);
    form.submit();
  };
  function swal_del(id) {
    swal({
      title: "Are you sure?",
      text: "You will not be able to recover this record data!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",   
      confirmButtonText: "Yes, delete it!",   
      closeOnConfirm: false 
    }, function(){  
      form_post("{!! url('posts') !!}/"+id, "DELETE") }
    );
  }

  function publish(opt, id) {
    window.location = '{!! url('posts/publish') !!}/'+id+'/'+opt.value;
  }
</script>
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="/js/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}
<script src="{{ asset ("/js/sweetalert.min.js") }}" type="text/javascript"></script>
@endsection
