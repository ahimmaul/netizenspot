@extends('layouts.app')

@section('content')
<section class="content-header">
  <h1>
    Account Setting
    <small>Basic Informartion</small>
  </h1>
</section>

<section class="content">
  @include('partials.error')
  @include('partials.message')
  <div class="row">
    <div class="col-md-12">
       <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Form Settings</h3>
        </div>
        {!! Form::open(['url' => 'settings/avatar', 'class' => 'form-horizontal', 'files' => true]) !!}
          <div class="box-body">
            <div class="form-group">
              {!! Form::label('photo', 'Photo', ['class' => 'col-sm-2 control-label']) !!}
              <div class="col-sm-10">
                @if (isset($user->avatar))
                <img src="{{ url('uploads/images/'.$user->avatar.'_160x160.'.$user->avatar_ext) }}"/>
                @else
                <img src="{{ url('uploads/images/user_160x160.png') }}"/>
                @endif
                {!! Form::file('photo', ['class' => 'form-control']) !!}
              </div>
            </div>
          </div>
          <div class="box-footer">
            {!! Form::submit('Update Avatar', ['class' => 'btn btn-success pull-right', 'name' => 'submit']) !!}
          </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</section>
@endsection

