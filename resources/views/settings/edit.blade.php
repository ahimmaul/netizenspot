@extends('layouts.app')

@section('content')
<section class="content-header">
  <h1>
    Account Setting
    <small>Basic Information</small>
  </h1>
</section>

<section class="content">
  @include('partials.error')
  @include('partials.message')
  <div class="row">
    <div class="col-md-12">
       <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Form Settings</h3>
        </div>
        {!! Form::open(['url' => 'settings/edit', 'class' => 'form-horizontal']) !!}
          <div class="box-body">
            <div class="form-group">
              {!! Form::label('name', 'Name', ['class' => 'col-sm-2 control-label']) !!}
              <div class="col-sm-10">
                {!! Form::text('name', Auth::user()->name, ['class' => 'form-control', 'required' => 'required']) !!}
              </div>
            </div>
            <div class="form-group">
              {!! Form::label('birthday', 'Birthday', ['class' => 'col-sm-2 control-label']) !!}
              <div class="col-sm-10">
                {!! Form::date('birthday', (isset($user->birthday) ? $user->birthday : ''), ['class' => 'form-control', 'required' => 'required']) !!}
              </div>
            </div>
            <div class="form-group">
              {!! Form::label('phone', 'Phone', ['class' => 'col-sm-2 control-label']) !!}
              <div class="col-sm-10">
                {!! Form::text('phone', (isset($user->phone) ? $user->phone : ''), ['class' => 'form-control', 'required' => 'required']) !!}
              </div>
            </div>
            <div class="form-group">
              {!! Form::label('address', 'Address', ['class' => 'col-sm-2 control-label']) !!}
              <div class="col-sm-10">
                {!! Form::textarea('address', (isset($user->address) ? $user->address : ''), ['class' => 'form-control', 'required' => 'required']) !!}
              </div>
            </div>
          </div>
          <div class="box-footer">
            {!! Form::submit('Update Profile', ['class' => 'btn btn-success pull-right', 'name' => 'submit']) !!}
          </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</section>
@endsection

