@extends('layouts.app')

@section('content')
<section class="content-header">
  <h1>
    Account Setting
    <small>Reset Password</small>
  </h1>
</section>

<section class="content">
  @include('partials.error')
  @include('partials.message')
  <div class="row">
    <div class="col-md-12">
       <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Form Settings</h3>
        </div>
        {!! Form::open(['url' => 'settings/password', 'class' => 'form-horizontal']) !!}
          <div class="box-body">
            @if(Auth::user()->password <> 'OAuth')
            <div class="form-group">
              {!! Form::label('password_old', 'Old Password', ['class' => 'col-sm-2 control-label']) !!}
              <div class="col-sm-10">
                {!! Form::password('password_old', ['class' => 'form-control', 'required' => 'required']) !!}
              </div>
            </div>
            @endif
            <div class="form-group">
              {!! Form::label('password', 'Password New', ['class' => 'col-sm-2 control-label']) !!}
              <div class="col-sm-10">
                {!! Form::password('password', ['class' => 'form-control', 'required' => 'required']) !!}
              </div>
            </div>
            <div class="form-group">
              {!! Form::label('password_confirmation', 'Password Confirmation', ['class' => 'col-sm-2 control-label']) !!}
              <div class="col-sm-10">
                {!! Form::password('password_confirmation', ['class' => 'form-control', 'required' => 'required']) !!}
              </div>
            </div>
          </div>
          <div class="box-footer">
            {!! Form::submit('Reset Password', ['class' => 'btn btn-success pull-right', 'name' => 'submit']) !!}
          </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</section>
@endsection