    <aside class="main-sidebar">
        <section class="sidebar">
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ url('canvas/160x160') }}" class="img-circle user-image" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a> • <a target="_blank" href="{{ url('') }}">Visit FrontEnd</a>
                </div>
            </div>
            <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview {{ str_contains(Request::path(),'posts') ? 'active' : '' }}">
              <a href="#">
                <i class="fa fa-calendar"></i> <span>Posts</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="{{ Request::is('posts') ? 'active' : '' }}"><a href="{{ url('posts') }}"><i class="fa fa-circle-o"></i> All Posts</a></li>
                <li class="{{ Request::is('posts/create') ? 'active' : '' }}"><a href="{{ url('posts/create') }}"><i class="fa fa-circle-o"></i> Add New</a></li>
              </ul>
            </li>
            <li class="treeview {{ str_contains(Request::path(),'settings') ? 'active' : '' }}">
              <a href="#">
                <i class="fa fa-cogs"></i> <span>Settings</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="{{ Request::is('settings/edit') ? 'active' : '' }}">
                  <a href="{{ url('settings/edit') }}">Basic Information</a>
                </li>
                <li class="{{ Request::is('settings/avatar') ? 'active' : '' }}">
                  <a href="{{ url('settings/avatar') }}">Change Avatar</a>
                </li>
                <li class="{{ Request::is('settings/password') ? 'active' : '' }}">
                  <a href="{{ url('settings/password') }}">Change Password</a>
                </li>
              </ul>
            </li>
          </ul>
        </section>
    </aside>