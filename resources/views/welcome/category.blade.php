@extends('layouts.welcome')

@section('content')
<section class="content">
    <div class="container">
        <div class="row">
           <div class="post-content post-modern col-md-9">
                @foreach($posts as $post)
                <div class="post-item">
                    <div class="post-image">
                        <a href="{{ $post->link }}">
                            <img alt="" src="{{ $post->photo }}">
                        </a>
                    </div>
                    <div class="post-content-details">
                        <div class="post-title">
                            <h3><a href="{{ $post->link }}">{{ $post->title }}</a></h3>
                        </div>
                        <div class="post-info">
                            <span class="post-autor">Posted by: <a href="#">{{ $post->user->name }}</a></span>
                        </div>
                        <div class="post-description">
                            <p>{{ $post->short_content }}</p>

                            <div class="post-read-more">
                                <a class="read-more" href="{{ $post->link }}">read more <i class="fa fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="post-meta">
                        <div class="post-date">
                            <span class="post-date-day">{{ $post->day }}</span>
                            <span class="post-date-month">{{ $post->month }}</span>
                            <span class="post-date-year">{{ $post->year }}</span>
                        </div>

                        <div class="post-comments">
                            <a href="#">
                                <i class="fa fa-comments-o"></i>
                                <span class="post-comments-number">0</span>
                            </a>
                        </div>
                        <div class="post-comments">
                            <a href="#">
                                <i class="fa fa-share-alt"></i>
                                <span class="post-comments-number">0</span>
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach

                <div class="text-center">
                    {{ $posts->links() }}
                </div>
            </div>
            <div class="sidebar sidebar-modern col-md-3" style="padding-right: 0px;">
                <div class="widget clearfix widget-newsletter">
                    <form id="widget-subscribe-form-sidebar" role="form" method="post" class="form-inline">
                        <h4 class="widget-title">Newsletter</h4>
                        <small>Stay informed on our latest news!</small>
                        <div class="input-group">
                            {{ csrf_field() }}
                            <input type="email" id="newsletter-email" aria-required="true" name="email" class="form-control required email" placeholder="Enter your Email" required="required">
                            <span class="input-group-btn">
                            <button type="submit" id="widget-subscribe-submit-button" class="btn btn-primary"><i class="fa fa-paper-plane"></i></button>
                            </span> 
                        </div>
                    </form>
                </div>

                <div class="widget clearfix widget-blog-articles">
                    <h4 class="widget-title">Popular Post</h4>
                    <ul class="list-posts list-medium">
                        @foreach($posts_popular as $post)
                        <li><a href="{{ $post->link }}">{{ $post->title }}</a>
                            <small>{{ $post->updated_at }}</small>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>

        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
$("#widget-subscribe-form-sidebar").submit(function(e) {
    var url = '{{ url('newsletter/subscribe') }}';
    $.ajax({
           type: "POST",
           url: url,
           data: $("#widget-subscribe-form-sidebar").serialize(), 
            success: function(text) {
                if (text.response == 'success') {
                    $.notify({
                        message: "You have successfully subscribed to our mailing list."
                    }, {
                        type: 'success'
                    });
                    $('#newsletter-email').val('');
                } else {
                    $.notify({
                        message: text.message
                    }, {
                        type: 'warning'
                    });
                }
            }
    });
    e.preventDefault(); // avoid to execute the actual submit of the form.
});
</script>
@endsection

@section('styles')
    <style type="text/css">
        .content {
            background-color: #ececec;
            padding-top: 20px;
            padding-bottom: 20px;
        }
        .post-content {
            background-color: #ffffff;
            box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
            padding-top: 20px;
        }
        .sidebar .widget {
            background-color: #ffffff;
            box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
            padding: 10px;
        }
        .heading {
            margin-bottom: 20px;
        }
        .row {
            margin-left: 0px;
            margin-right: 0px;
        }
    </style>
@endsection