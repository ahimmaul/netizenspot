@extends('layouts.welcome_mobile')

@section('content')
        <section class="p-t-0 p-b-0 background-gray">
            <div class="container" style="background-color: #fff;">
                <div class="top-owl">
                    <div class="row">
                        <div class="col-md-8" style="width: 100%;">
                            <div class="owl-carousel owl-theme" style="">
                                @foreach ($posts_featured as $post)
                                <article class="post-entry">
                                    <a href="{{ $post->link }}" class="post-image"><img alt="{{ $post->title }}" class="owl-lazy" data-src="{{ $post->photo }}"></a>
                                    <div class="post-entry-overlay">
                                        <div class="post-datetime">
                                            {{ $post->datetime }}
                                        </div>
                                        <div class="post-title">
                                            <a href="{{ $post->link }}">{{ $post->title }}</a>
                                        </div>
                                        <div class="post-category">
                                            {{ $post->category }}
                                        </div>
                                        <div class="post-short_content">
                                            <a href="{{ $post->link }}">{{ $post->short_content }}</a>
                                        </div>
                                        <div class="post-linked">
                                            <a href="{{ $posts_latest[$post->cat_id][0]->link }}"><em>{{ $posts_latest[$post->cat_id][0]->title }}</em></a>
                                        </div>
                                    </div>
                                </article>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-md-4" style="height: 100%;width: 100%">
                            <p class="text-muted" id="top-datetime" style="padding: 20px 20px 0px 10px; margin-bottom: 0px">{{ $posts_featured[0]->datetime }}</p>
                            <h2 id="top-title" style="padding: 0px 20px 0px 10px;font-weight: bold;line-height: 120% !important;"><a href="{{ $post->link }}">{{ $posts_featured[0]->title }}</a></h2>
                            <span id="top-category" class="label label-danger" style="margin-left: 10px;">{{ $posts_featured[0]->category }}</span>
                            <p id="top-short_content" style="padding: 10px 20px 20px 10px"><a href="{{ $post->link }}">{{ $posts_featured[0]->short_content }}</a></p>

                            <div class="bottom-details" style="position: relative;bottom: 10px;font-size: 120%;padding-left: 10px; padding-right: 20px">
                                <div class="seperator seperator-medium" style="width: 100%;"></div>
                                <strong>Berita Terkait</strong><br>
                                <div id="top-linked">
                                    <a href="{{ $posts_latest[$posts_featured[0]->cat_id][0]->link }}"><em>{{ $posts_latest[$posts_featured[0]->cat_id][0]->title }}</em></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="p-t-20 p-b-0 background-gray">
            <div class="container">
                <div class="row">
                    <div class="panel-1">
                        <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4>Berita Terbaru</h4>
                        </div>
                        <div class="panel-body post-thumbnail-list">
                        @foreach($posts_berita as $post)
                            <div class="post-thumbnail-entry">
                                <img alt="" src="{{ $post->photo }}"  width="93" height="93">
                                <div class="post-thumbnail-content">
                                    <h4><a href="{{ $post->link }}">{{ $post->title }}</a></h4>
                                    <span class="post-date"><i class="fa fa-clock-o"></i> {{ $post->datetime }}</span>
                                </div>
                            </div>
                        @endforeach
                        </div>
                        </div>
                    </div>
                    <div class="panel-2">
                        <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Pilihan</h4>
                        </div>
                        <div class="panel-body post-thumbnail-list">
                            @foreach($posts_featured_last as $post)
                            <div class="post-thumbnail-entry">
                                @if($loop->iteration == 1)
                                    <h4><strong><a href="{{ $post->link }}">{{ $post->title }}</a></strong></h4>
                                    <div class="row">
                                        <img alt="" src="{{ $post->photo }}"  width="100%" height="auto">
                                    </div>
                                    <div class="post-thumbnail-content">
                                        <span class="post-date">{{ $post->datetime }}</span>
                                        <h4><a href="{{ $post->link }}">{{ $post->short_content }}</a></h4>
                                    </div>
                                @else
                                    <img alt="" src="{{ $post->photo }}"  width="93" height="93">
                                    <div class="post-thumbnail-content">
                                        <h4><a href="{{ $post->link }}">{{ $post->title }}</a></h4>
                                        <span class="post-date"><i class="fa fa-clock-o"></i> {{ $post->datetime }}</span>
                                    </div>
                                @endif
                            </div>
                            @endforeach
                        </div>
                        </div>
                    </div>
                    <div class="panel-3">
                        <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4>Terpopuler</h4></div>
                        <div class="panel-body post-thumbnail-list">
                            @foreach($posts_popular as $post)
                            <div class="post-thumbnail-entry">
                                <li class="num">0{{ $loop->iteration }}</li>
                                <div class="post-thumbnail-content" style="padding-left: 40px;">
                                    <span class="post-date">{{ $post->datetime }}</span>
                                    <h4><a href="{{ $post->link }}">{{ $post->title }}</a></h4>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        </div>
                    </div>
                    <div class="panel-4">
                        <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4>VIDEO</h4>
                        </div>
                        <div class="panel-body post-thumbnail-list">
                            @foreach($posts_video as $post)
                            @if($loop->iteration == 1 or $loop->iteration == 4)
                            <div class="row">
                            @endif
                                <div class="col-md-6" style="position: relative;">
                                    <a href="{{ $post->link }}">
                                        <img src="{{ $post->photo }}" title="{{ $post->title }}" width="100%" height="auto">
                                        <div class="icon_play"></div>
                                    </a>
                                    <span class="post-date" style="color: #999;font-size: 13px;">{{ $post->datetime }}</span>
                                    <h5><a href="{{ $post->link }}">{{ $post->title }}</a></h5>
                                </div>
                            @if($loop->iteration == 3 or $loop->iteration == 6)
                            </div>
                            @endif
                            @endforeach
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="p-t-0 p-b-0 background-gray">
            <div class="container">
            <div class="row ads">
                <div class="col-md-6">
                    <img src="{{ url('ads/adglow_562x88.jpg') }}" width="100%">        
                </div>
                <div class="col-md-6">
                    <img src="{{ url('ads/apf_562x88.jpg') }}" width="100%">  
                </div>
            </div>
            </div>
        </section>
        <section class="p-t-0 p-b-0 background-gray">
            <div class="container">
                <div class="row">
                    <div class="panel-5">
                        <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Kebudayaan</h4>
                        </div>
                        <div class="panel-body post-thumbnail-list">
                        @foreach($posts_latest[2] as $post)
                            <div class="post-thumbnail-entry">
                                <img alt="" src="{{ $post->photo }}">
                                <div class="post-thumbnail-content">
                                    <h4><a href="{{ $post->link }}">{{ $post->title }}</a></h4>
                                    <span class="post-date"><i class="fa fa-clock-o"></i> {{ $post->updated_at }}</span>
                                </div>
                            </div>
                        @endforeach
                        </div>
                        </div>
                    </div>
                    <div class="panel-6">
                        <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>IPTEK</h4>
                        </div>
                        <div class="panel-body post-thumbnail-list">
                            @foreach($posts_latest[4] as $post)
                            <div class="post-thumbnail-entry">
                                <img alt="" src="{{ $post->photo }}">
                                <div class="post-thumbnail-content">
                                    <h4><a href="{{ $post->link }}">{{ $post->title }}</a></h4>
                                    <span class="post-date"><i class="fa fa-clock-o"></i> {{ $post->updated_at }}</span>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        </div>
                    </div>
                    <div class="panel-7">
                        <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4>LIVE</h4>
                        </div>
                        <div class="panel-body post-thumbnail-list">
                            @foreach($posts_latest[3] as $post)
                            @if($loop->iteration == 1)
                                <h4><strong><a href="{{ $post->link }}">{{ $post->title }}</a></strong></h4>
                                <div class="row">
                                    <iframe width="100%" height="auto" src="https://www.youtube.com/embed/J_kaLdz4GZk" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="post-thumbnail-content">
                                    <span class="post-date">{{ $post->updated_at }}</span>
                                    <h4><a href="{{ $post->link }}" style="font-size: 15px">{{ $post->short_content }}</a></h4>
                                </div>
                            @else
                            @endif
                            @endforeach
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="p-t-0 p-b-0 background-gray">
            <div class="container">
            <div class="row ads">
                <div class="col-md-6">
                    <img src="{{ url('ads/cumi_562x88.jpg') }}" width="100%">        
                </div>
                <div class="col-md-6">
                    <img src="{{ url('ads/persada_562x88.jpg') }}" width="100%">  
                </div>
            </div>
            </div>
        </section>
        <section class="p-t-0 p-b-0 background-gray">
            <div class="container">
                <div class="row">
                    <div class="panel-8">
                        <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Hiburan</h4>
                        </div>
                        <div class="panel-body post-thumbnail-list">
                            @foreach($posts_latest[5] as $post)
                            <div class="post-thumbnail-entry">
                                <img alt="" src="{{ $post->photo }}">
                                <div class="post-thumbnail-content">
                                    <h4><a href="{{ $post->link }}">{{ $post->title }}</a></h4>
                                    <span class="post-date"><i class="fa fa-clock-o"></i> {{ $post->updated_at }}</span>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        </div>
                    </div>
                    <div class="panel-9">
                        <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Kesehatan</h4>
                        </div>
                        <div class="panel-body post-thumbnail-list">
                            @foreach($posts_latest[6] as $post)
                            <div class="post-thumbnail-entry">
                                <img alt="" src="{{ $post->photo }}">
                                <div class="post-thumbnail-content">
                                    <h4><a href="{{ $post->link }}">{{ $post->title }}</a></h4>
                                    <span class="post-date"><i class="fa fa-clock-o"></i> {{ $post->updated_at }}</span>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        </div>
                    </div>
                    <div class="panel-10">
                        <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Olahraga</h4>
                        </div>
                        <div class="panel-body post-thumbnail-list">
                            @foreach($posts_latest[7] as $post)
                            <div class="post-thumbnail-entry">
                                <img alt="" src="{{ $post->photo }}">
                                <div class="post-thumbnail-content">
                                    <h4><a href="{{ $post->link }}">{{ $post->title }}</a></h4>
                                    <span class="post-date"><i class="fa fa-clock-o"></i> {{ $post->updated_at }}</span>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="p-t-0 p-b-0 background-gray">
            <div class="container">
            <div class="row ads">
                <div class="col-md-6">
                    <img src="{{ url('ads/phicos_562x88.jpg') }}" width="100%">        
                </div>
                <div class="col-md-6">
                    <img src="{{ url('ads/superholiday_562x88.jpg') }}" width="100%">  
                </div>
            </div>
            </div>
        </section>
        <section class="p-t-0 p-b-0 background-gray">
            <div class="container">
                <div class="row">
                    <div class="panel-11">
                        <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Kuliner</h4>
                        </div>
                        <div class="panel-body post-thumbnail-list">
                            @foreach($posts_latest[8] as $post)
                            <div class="post-thumbnail-entry">
                                <img alt="" src="{{ $post->photo }}">
                                <div class="post-thumbnail-content">
                                    <h4><a href="{{ $post->link }}">{{ $post->title }}</a></h4>
                                    <span class="post-date"><i class="fa fa-clock-o"></i> {{ $post->updated_at }}</span>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        </div>
                    </div>
                    <div class="panel-12">
                        <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Travel</h4>
                        </div>
                        <div class="panel-body post-thumbnail-list">
                            @foreach($posts_latest[9] as $post)
                            <div class="post-thumbnail-entry">
                                <img alt="" src="{{ $post->photo }}">
                                <div class="post-thumbnail-content">
                                    <h4><a href="{{ $post->link }}">{{ $post->title }}</a></h4>
                                    <span class="post-date"><i class="fa fa-clock-o"></i> {{ $post->updated_at }}</span>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        </div>
                    </div>
                    <div class="panel-13">
                        <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Komunitas</h4>
                        </div>
                        <div class="panel-body post-thumbnail-list">
                            @foreach($posts_latest[10] as $post)
                            <div class="post-thumbnail-entry">
                                <img alt="" src="{{ $post->photo }}">
                                <div class="post-thumbnail-content">
                                    <h4><a href="{{ $post->link }}">{{ $post->title }}</a></h4>
                                    <span class="post-date"><i class="fa fa-clock-o"></i> {{ $post->updated_at }}</span>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ url('bower_components/owl.carousel/dist/assets/owl.carousel.min.css') }}" />
    <style type="text/css">
        .background-gray {
            background-color: #ececec;
        }
        .background-white {
            background-color: #ffffff;
            box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
        }
        section {
            padding: 20px 0;
        }
        .owl-carousel .post-entry .post-entry-overlay {
            position:absolute;
            color:#fff;
            font-size:12px;
            display:block;
        }
        .owl-dots {
          position: absolute;
          right: 0;
          top: 0px;
          margin-top: 2px !important;
        }
        .num {
            list-style: none;
            float: left;
            font-family: arial;
            font-style: italic;
            color: #c4c3c3;
            font-size: 20px;
            font-weight: normal;
            padding: 10px 10px 10px 0px;
        }
        .icon_play {
            background: url({{ url('images/ico_video.png') }}) no-repeat scroll center center;
            background-color: rgba(0,0,0,0.7);
            background-size: 25px;
            height: 30px;
            width: 30px;
            opacity: 0.8;
            position: absolute;
            top: 0px;
        }
        #top-linked a em {
            font-size: 15px;
        }
        .ads {
            margin-bottom: 20px;
        }
    </style>
@endsection

@section('scripts')
    <script src="{{ url('bower_components/owl.carousel/dist/owl.carousel.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            var owl = $('.owl-carousel');
            owl.owlCarousel({
                items: 1,
                singleItem: true,
                lazyLoad: true,
                autoplay : true,
                loop: true
            });
            // Listen to owl events:
            owl.on('changed.owl.carousel', function(property) {
                var current = property.item.index;
                var category = $.trim($(property.target).find(".owl-item").eq(current).find(".post-category").html());
                var title = $.trim($(property.target).find(".owl-item").eq(current).find(".post-title").html());
                var datetime = $.trim($(property.target).find(".owl-item").eq(current).find(".post-datetime").html());
                var short_content = $.trim($(property.target).find(".owl-item").eq(current).find(".post-short_content").html());
                var linked = $.trim($(property.target).find(".owl-item").eq(current).find(".post-linked").html());
                $('#top-category').html(category);
                $('#top-title').html(title);
                $('#top-datetime').html(datetime);
                $('#top-short_content').html(short_content);
                $('#top-linked').html(linked);
            })
          
        });
    </script>
@endsection