<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/', 'WelcomeController@index');
Route::resource('posts', 'PostController');
Route::get('posts/publish/{id}/{is_publish}', 'PostController@publish')->where(['is_publish' => '[0-2]+']);
Route::get('uploads/images/{filename}_{width}x{height}.{extension}', function ($filename, $width, $height, $extension)
{
	$allowed_extension = ['jpeg', 'jpg', 'png'];
	if (!in_array($extension, $allowed_extension)) {
		return 'extension not supported';
	}
  $filepath = storage_path('app/images/'.$filename.'.'.$extension);

  $watermarkpath = storage_path('app/public/watermark.png');
  $watermark = Image::cache(function($image) use ($watermarkpath) {
    $image = $image->make($watermarkpath);

    return $image->resize(140, 22);
  }, 1440, true);

  $img = Image::cache(function($image) use ($filepath, $watermark, $width, $height) {
    $image = $image->make($filepath);
    $image->insert($watermark, 'bottom-right', 5, 5);

    return $image->resize($width, $height);
  }, 1440, true);

  return $img->response();
})->where(['width' => '[0-9]+', 'height' => '[0-9]+']);

Route::get('canvas/{width}x{height}', function ($width, $height)
{
  $img = Image::cache(function($image) use ($width, $height) {
    $image = $image->canvas($width, $height, '#283593');
    return $image->encode('png');
  }, 1440, true);

  $response = Response::make($img);
  $response->header('Content-Type', 'image/png');
  return $response;
})->where(['width' => '[0-9]+', 'height' => '[0-9]+']);

Route::get('static/images/{filename}_{width}x{height}.{extension}', function ($filename, $width, $height, $extension)
{
  $allowed_extension = ['jpeg', 'jpg', 'png'];
  if (!in_array($extension, $allowed_extension)) {
    return 'extension not supported';
  }
  $filepath = storage_path('app/public/'.$filename.'.'.$extension);

  $img = Image::cache(function($image) use ($filepath, $width, $height) {
    $image = $image->make($filepath);

    return $image->resize($width, $height);
  }, 1440, true);

  return $img->response();
})->where(['width' => '[0-9]+', 'height' => '[0-9]+']);

Route::get('ads/{filename}_{width}x{height}.{extension}', function ($filename, $width, $height, $extension)
{
  $allowed_extension = ['jpeg', 'jpg', 'png'];
  if (!in_array($extension, $allowed_extension)) {
    return 'extension not supported';
  }
  $filepath = storage_path('app/ads/'.$filename.'.'.$extension);

  $img = Image::cache(function($image) use ($filepath, $width, $height) {
    $image = $image->make($filepath);

    return $image->resize($width, $height);
  }, 1440, true);

  return $img->response();
})->where(['width' => '[0-9]+', 'height' => '[0-9]+']);

Route::get('pusher', function() {
	$options = array(
    'encrypted' => true
  );
  $pusher = new Pusher(
    '5213c51b66f42c051324',
    'bbeb43f3b073cf929d91',
    '282523',
    $options
  );

  $data['message'] = 'hello world';
  $pusher->trigger('my-channel', 'my-event', $data);
});

Route::get('auth/fb', 'FbAuthController@redirectToProvider');
Route::get('auth/fb/callback', 'FbAuthController@handleProviderCallback');
Route::get('auth/google', 'GAuthController@redirectToProvider');
Route::get('auth/google/callback', 'GAuthController@handleProviderCallback');

Route::get('message/{id}', 'MessageController@chatHistory')->name('message.read');
Route::group(['prefix'=>'ajax', 'as'=>'ajax::'], function() {
   Route::post('message/send', 'MessageController@ajaxSendMessage')->name('message.new');
   Route::delete('message/delete/{id}', 'MessageController@ajaxDeleteMessage')->name('message.delete');
});

Route::get('test', function() {
  $query = DB::table('posts')->get();
  foreach ($query as $data) {
    echo $data->slug;
  }
});

Route::get('settings/edit', 'SettingController@edit');
Route::post('settings/edit', 'SettingController@update');
Route::get('settings/avatar', 'SettingController@avatar');
Route::post('settings/avatar', 'SettingController@avatar_update');
Route::get('settings/password', 'SettingController@password');
Route::post('settings/password', 'SettingController@password_update');

Route::get('post/{post}', 'WelcomeController@post');
Route::get('category/{cat}', 'WelcomeController@category');
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->middleware('auth');

Route::post('newsletter/subscribe', 'NewsletterController@subscribe');
Route::get('analytics', 'AnalyticController@index');

Route::get('info', function() {
  phpinfo();
});