let mix = require('laravel-mix').mix;

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */

mix.sass('resources/assets/sass/app.scss', 'public/css');
mix.combine([
		'resources/assets/bower/owl.carousel/dist/assets/owl.carousel.css',
		'resources/assets/bower/magnific-popup/dist/magnific-popup.css',
		'resources/assets/bower/font-awesome/css/font-awesome.css',
		'resources/assets/bower/template/css/theme-base.css',
		'resources/assets/bower/template/css/theme-elements.css',
		'resources/assets/bower/template/css/red-dark.css',
		'resources/assets/bower/template/css/custom.css'
	], 'public/css/all.css');

//mix.js('resources/assets/js/app.js', 'public/js');

mix.disableNotifications();
//mix.version();